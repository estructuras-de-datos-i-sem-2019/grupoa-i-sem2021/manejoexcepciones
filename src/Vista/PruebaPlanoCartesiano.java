/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.PlanoCartesiano;

/**
 *
 * @author madar
 */
public class PruebaPlanoCartesiano {
    
    public static void main(String[] args) {
        String url="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv";
        PlanoCartesiano myPlano=new PlanoCartesiano(url);
        System.out.println("Mi plano es:\n"+myPlano.toString());
    }
    
}
